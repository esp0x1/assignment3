﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Assignment3.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", maxLength: 255, nullable: false),
                    Director = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Trailer = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "Sully", "Male", "Jake Sully", "static.wikia.nocookie.net/p__/images/e/e9/250px-Humansully.jpg/revision/latest?cb=20120114175238&path-prefix=protagonist" },
                    { 2, "", "Female", "Neytiri", "static.wikia.nocookie.net/p__/images/4/4a/Neytiri.jpg/revision/latest/scale-to-width-down/350?cb=20201210221356&path-prefix=protagonist" },
                    { 3, "Mr. Underhill", "Male", "Frodo Baggins", "static.wikia.nocookie.net/lotr/images/1/1a/FotR_-_Elijah_Wood_as_Frodo.png/revision/latest/scale-to-width-down/350?cb=20130313174543" },
                    { 4, "Sméagol", "Male", "Gollum", "static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509" },
                    { 5, "Istar", "Male", "Gandalf", "static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest/scale-to-width-down/350?cb=20121110131754" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "James Cameron's Avatar franchise is a planned series of science fiction films produced by Cameron's Lightstorm Entertainment and distributed by 20th Century Studios, as well as associated computer games and theme park rides.", "Avatar" },
                    { 2, "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King.", "The Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "James Cameron", 1, "Action,Adventure,Fantasy", "www.imdb.com/title/tt0499549/mediaviewer/rm371527425/?ref_=tt_ov_i", 2009, "Avatar", "www.imdb.com/video/vi531039513?playlistId=tt0499549&ref_=tt_ov_vi" },
                    { 2, "James Cameron", 1, "Action,Adventure,Sci-fi", "www.imdb.com/title/tt1630029/mediaviewer/rm1900810753/?ref_=tt_ov_i", 2022, "Avatar: The Way of Water", "www.imdb.com/video/vi2659435033?playlistId=tt1630029&ref_=tt_ov_vi" },
                    { 3, "Peter Jackson", 2, "Action,Adventure,Drama", "www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i", 2001, "The Lord of the Rings: The Fellowship of the Ring", "www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_ov_vi" },
                    { 4, "Peter Jackson", 2, "Action,Adventure,Drama", "www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i", 2002, "The Lord of the Rings: The Two Towers", "www.imdb.com/video/vi701350681?playlistId=tt0167261&ref_=tt_ov_vi" },
                    { 5, "Peter Jackson", 2, "Action,Adventure,Drama", "www.imdb.com/title/tt0167260/mediaviewer/rm584928512/?ref_=tt_ov_i", 2003, "The Lord of the Rings: The Return of the King", "www.imdb.com/video/vi718127897?playlistId=tt0167260&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 3 },
                    { 5, 3 },
                    { 3, 4 },
                    { 4, 4 },
                    { 5, 4 },
                    { 3, 5 },
                    { 4, 5 },
                    { 5, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
