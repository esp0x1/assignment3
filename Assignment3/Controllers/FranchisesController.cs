﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using Assignment3.Models.Domain;
using Assignment3.Services.FranchiseServices;
using AutoMapper;
using Assignment3.Models.DTOs.MovieDTOs;
using Assignment3.Models.DTOs.FranchiseDTOs;
using Assignment3.Models.DTOs.CharacterDTOs;
using System.Net.Mime;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets all the franchises in the MovieCharacterDb
        /// </summary>
        /// <returns>A list of all franchises</returns>
        /// <response code="200">Returned all franchises successfully</response>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetFranchises());
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Gets a franchise in the MovieCharacterDb based on an ID
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <returns>An franchise</returns>
        /// <response code="200">Returned specific franchise successfully</response>
        /// <response code="404">Franchise not found</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetFranchise(id);
            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Updates the franchise based on ID and data related to the franchise
        /// </summary>
        /// <param name="id">The ID of the franchise to update </param>
        /// <param name="dtoFranchise">The franchise to update</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/Franchises/1
        ///     {
        ///        "id": 1,
        ///        "name": "Star Wars",
        ///        "description": "Good vs evil in space"
        ///     }
        ///
        /// </remarks>
        /// <response code="204">Successfully updated character</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Character not found</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.PutFranchise(domainFranchise);

            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="dtoFranchise">The franchise to create</param>
        /// <returns>The created franchise</returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/Franchises/
        ///     {
        ///        "name": "Star Wars",
        ///        "description": "Good vs evil in space"
        ///     }
        ///
        /// </remarks>
        /// <response code="201">Successfully added franchise</response>
        /// <response code="400">Error handling the data input</response>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            domainFranchise = await _franchiseService.AddFranchise(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes a franchise based on ID
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <response code="204">Successfully deleted franchise</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Franchise not found</response>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchise(id);

            return NoContent();
        }

        // GET: api/Franchises/1/Characters
        /// <summary>
        /// Gets the characters in a specific franchise
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <returns>The characters in the franchise</returns>
        /// <response code="200">Returned characters in specific franchise successfully</response>
        /// <response code="404">Franchise not found</response>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetFranchiseCharacters(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetCharactersInFranchise(id));
        }
        // GET: api/Franchises/1/Movies
        /// <summary>
        /// Gets the movies in a specific franchise
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <returns>The movies in the franchise</returns>
        /// <response code="200">Returned movies in specific franchise successfully</response>
        /// <response code="404">Franchise not found</response>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetFranchiseMovies(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetMoviesInFranchise(id));
        }


        // PUT: api/Franchises/1/Movies
        /// <summary>
        /// Updates the movies in the franchise
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <param name="movieIds">The IDs of the movies</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/Franchises/1/Movies
        ///     [
        ///         1,2,3
        ///     ]
        ///
        /// </remarks>
        /// <response code="204">Successfully updated the movies in a franchise</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Franchise not found</response>
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> PutFranchiseMovies(int id, int[] movieIds)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateMoviesInFranchise(id, movieIds);
            }
            catch(KeyNotFoundException ex)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }
}
