﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using Assignment3.Services.CharacterServices;
using Assignment3.Models.Domain;
using AutoMapper;
using Assignment3.Models.DTOs.CharacterDTOs;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System.Net.Mime;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all the characters in the MovieCharacterDb
        /// </summary>
        /// <returns>A list of all characters</returns>
        /// <response code="200">Returned all characters successfully</response>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetCharacters());
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets a character in the MovieCharacterDb based on an ID
        /// </summary>
        /// <param name="id">The ID of the character</param>
        /// <returns>An character</returns>
        /// <response code="200">Returned specific character successfully</response>
        /// <response code="404">Character not found</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _characterService.GetCharacter(id);
            
            if (character == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Updates the character based on ID and data related to the character
        /// </summary>
        /// <param name="id">The ID of the character to update</param>
        /// <param name="dtoCharacter">The character to update</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/Characters/1
        ///     {
        ///        "id": 1,
        ///        "name": "John",
        ///        "alias": "Leader",
        ///        "gender": "Male",
        ///        "picture": "www.example.com"
        ///     }
        ///
        /// </remarks>
        /// <response code="204">Successfully updated character</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Character not found</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterService.PutCharacter(domainCharacter);
            return NoContent();
        }

        // POST: api/Characters
        /// <summary>
        /// Creates a new character
        /// </summary>
        /// <param name="dtoCharacter">The character to create</param>
        /// <returns>The created character</returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/Characters/
        ///     {
        ///        "name": "John",
        ///        "alias": "Leader",
        ///        "gender": "Male",
        ///        "picture": "www.example.com"
        ///     }
        ///
        /// </remarks>
        /// <response code="201">Successfully added character</response>
        /// <response code="400">Error handling the data input</response>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            domainCharacter = await _characterService.AddCharacter(domainCharacter);

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }
        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes a character based on ID
        /// </summary>
        /// <param name="id">The ID of the character</param>
        /// <response code="204">Successfully deleted character</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Character not found</response>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacter(id);

            return NoContent();
        }
    }
}
