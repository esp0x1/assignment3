﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using Assignment3.Models.Domain;
using Assignment3.Services.MovieServices;
using AutoMapper;
using Assignment3.Models.DTOs.MovieDTOs;
using Assignment3.Models.DTOs.CharacterDTOs;
using System.Net.Mime;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Gets all the movies in the MovieCharacterDb
        /// </summary>
        /// <returns>A list of all movies</returns>
        /// <response code="200">Returned all movies successfully</response>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetMovies());
        }

        // GET: api/Movies/5
        /// <summary>
        /// Gets a movie in the MovieCharacterDb based on an ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        /// <returns>A movie</returns>
        /// <response code="200">Returned specific movie successfully</response>
        /// <response code="404">Movie not found</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetMovie(id);
            if(movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }

        // PUT: api/Movies/5
        /// <summary>
        /// Updates the movie based on ID and data related to the character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/Movies/1
        ///     {
        ///        "id": 1,
        ///        "title": "Johnny English's adventure",
        ///        "genre": "Action,Drama",
        ///        "releaseyear": 2005,
        ///        "director": "Johnny English",
        ///        "picture": "www.example.com",
        ///        "trailer": "www.example.com"
        ///     }
        ///
        /// </remarks>
        /// <response code="204">Successfully updated movie</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Movie not found</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.PutMovie(domainMovie);

            return NoContent();
        }

        // POST: api/Movies
        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="dtoMovie">The movie to create</param>
        /// <returns>The created movie</returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST api/Movies/
        ///     {
        ///        "title": "Johnny English's adventure",
        ///        "genre": "Action,Drama",
        ///        "releaseyear": 2005,
        ///        "director": "Johnny English",
        ///        "picture": "www.example.com",
        ///        "trailer": "www.example.com"
        ///     }
        ///
        /// </remarks>
        /// <response code="201">Successfully added movie</response>
        /// <response code="400">Error handling the data input</response>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            domainMovie = await _movieService.AddMovie(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Deletes a movie based on an ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        /// <returns>The deleted movie</returns>
        /// <response code="204">Successfully deleted move</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Movie not found</response>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovie(id);

            return NoContent();
        }

        // GET: api/Movies/1/Characters
        /// <summary>
        /// Gets the characters in specific movie
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        /// <returns>The characters in the movie</returns>
        /// <response code="200">Returned characters in specific movie successfully</response>
        /// <response code="404">Movie not found</response>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetMovieCharacters(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetCharactersInMovie(id));
        }

        // PUT: api/Movies/1/Characters
        /// <summary>
        /// Updates the characters in a movie
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        /// <param name="characterIds">The character IDs</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT api/Movies/1
        ///     [ 
        ///         1,2,4
        ///     ]
        ///
        /// </remarks>
        /// <response code="204">Successfully updated movie</response>
        /// <response code="400">Error handling the data input</response>
        /// <response code="404">Movie not found</response>
        [HttpPut("{id}/Characters")]
        public async Task<IActionResult> PutMovieCharacters(int id, int[] characterIds)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            try
            {
                await _movieService.UpdateCharactersInMovie(id, characterIds);
            }
            catch(KeyNotFoundException ex)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }
}
