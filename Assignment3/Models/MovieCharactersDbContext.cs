﻿using Assignment3.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Models
{
    public class MovieCharactersDbContext : DbContext
    {
        // Tables
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }

        public MovieCharactersDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Makes it possible to delete a franchise
            modelBuilder.Entity<Franchise>()
                .HasMany(e => e.Movies)
                .WithOne(e => e.Franchise)
                .OnDelete(DeleteBehavior.SetNull);

            // Seed data for Franchise, Movie and Character

            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeed());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeed());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeed());

            // Seed data for m2m relationship 

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 3 },
                            new { MovieId = 5, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 },
                            new { MovieId = 4, CharacterId = 4 },
                            new { MovieId = 5, CharacterId = 4 },
                            new { MovieId = 3, CharacterId = 5 },
                            new { MovieId = 4, CharacterId = 5 },
                            new { MovieId = 5, CharacterId = 5 }
                            );
                    });
        }
    }
}
