﻿using Assignment3.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace Assignment3.Models
{
    /// <summary>
    /// Seed helper class for seed data
    /// </summary>
    public class SeedHelper
    {

        /// <summary>
        /// Gets Character seed data
        /// </summary>
        /// <returns>A list of Character seed data</returns>
        public static List<Character> GetCharacterSeed()
        {
            List<Character> characters = new List<Character>();

            characters.Add(new Character
            {
                Id = 1,
                Name = "Jake Sully",
                Alias = "Sully",
                Gender = "Male",
                Picture = "static.wikia.nocookie.net/p__/images/e/e9/250px-Humansully.jpg/revision/latest?cb=20120114175238&path-prefix=protagonist",
            });

            characters.Add(new Character
            {
                Id = 2,
                Name = "Neytiri",
                Alias = "",
                Gender = "Female",
                Picture = "static.wikia.nocookie.net/p__/images/4/4a/Neytiri.jpg/revision/latest/scale-to-width-down/350?cb=20201210221356&path-prefix=protagonist",
            });


            characters.Add(new Character
            {
                Id = 3,
                Name = "Frodo Baggins",
                Alias = "Mr. Underhill",
                Gender = "Male",
                Picture = "static.wikia.nocookie.net/lotr/images/1/1a/FotR_-_Elijah_Wood_as_Frodo.png/revision/latest/scale-to-width-down/350?cb=20130313174543",
            });

            characters.Add(new Character
            {
                Id = 4,
                Name = "Gollum",
                Alias = "Sméagol",
                Gender = "Male",
                Picture = "static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509",
            });


            characters.Add(new Character
            {
                Id = 5,
                Name = "Gandalf",
                Alias = "Istar",
                Gender = "Male",
                Picture = "static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest/scale-to-width-down/350?cb=20121110131754",
            });

            return characters;
        }

        /// <summary>
        /// Gets Franchise seed data
        /// </summary>
        /// <returns>A list of Franchise seed data</returns>
        public static List<Franchise> GetFranchiseSeed()
        {
            List<Franchise> franchises = new List<Franchise>();

            franchises.Add(new Franchise()
            {
                Id = 1,
                Name = "Avatar",
                Description = "James Cameron's Avatar franchise is a planned series of science fiction films produced by Cameron's Lightstorm Entertainment and distributed by 20th Century Studios, as well as associated computer games and theme park rides."
            });

            franchises.Add(new Franchise()
            {
                Id = 2,
                Name = "The Lord of the Rings",
                Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King."
            });

            return franchises;
        }

        /// <summary>
        /// Gets Movie seed data
        /// </summary>
        /// <returns>A list of Movie seed data</returns>
        public static List<Movie> GetMovieSeed()
        {
            List<Movie> movies = new List<Movie>();

            movies.Add(new Movie
            {
                Id = 1,
                Title = "Avatar",
                Genre = "Action,Adventure,Fantasy",
                ReleaseYear = 2009,
                Director = "James Cameron",
                Picture = "www.imdb.com/title/tt0499549/mediaviewer/rm371527425/?ref_=tt_ov_i",
                Trailer = "www.imdb.com/video/vi531039513?playlistId=tt0499549&ref_=tt_ov_vi",
                FranchiseId = 1,
            });

            movies.Add(new Movie
            {
                Id = 2,
                Title = "Avatar: The Way of Water",
                Genre = "Action,Adventure,Sci-fi",
                ReleaseYear = 2022,
                Director = "James Cameron",
                Picture = "www.imdb.com/title/tt1630029/mediaviewer/rm1900810753/?ref_=tt_ov_i",
                Trailer = "www.imdb.com/video/vi2659435033?playlistId=tt1630029&ref_=tt_ov_vi",
                FranchiseId = 1,
            });


            movies.Add(new Movie
            {
                Id = 3,
                Title = "The Lord of the Rings: The Fellowship of the Ring",
                Genre = "Action,Adventure,Drama",
                ReleaseYear = 2001,
                Director = "Peter Jackson",
                Picture = "www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i",
                Trailer = "www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_ov_vi",
                FranchiseId = 2,
            });

            movies.Add(new Movie
            {
                Id = 4,
                Title = "The Lord of the Rings: The Two Towers",
                Genre = "Action,Adventure,Drama",
                ReleaseYear = 2002,
                Director = "Peter Jackson",
                Picture = "www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i",
                Trailer = "www.imdb.com/video/vi701350681?playlistId=tt0167261&ref_=tt_ov_vi",
                FranchiseId = 2,
            });

            movies.Add(new Movie
            {
                Id = 5,
                Title = "The Lord of the Rings: The Return of the King",
                Genre = "Action,Adventure,Drama",
                ReleaseYear = 2003,
                Director = "Peter Jackson",
                Picture = "www.imdb.com/title/tt0167260/mediaviewer/rm584928512/?ref_=tt_ov_i",
                Trailer = "www.imdb.com/video/vi718127897?playlistId=tt0167260&ref_=tt_ov_vi",
                FranchiseId = 2,
            });

            return movies;
        }

    }
}
