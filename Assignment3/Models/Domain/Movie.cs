﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models.Domain
{
    public class Movie
    {
        // PK
        public int Id { get; set; }
        // Fields
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }
        [MaxLength(255)]
        [Required]
        public string Genre { get; set; }
        [MaxLength(255)]
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(255)]
        [Required]
        public string Director { get; set; }
        [MaxLength(255)]
        [Required]
        public string Picture { get; set; }
        [MaxLength(255)]
        [Required]
        public string Trailer { get; set; }
        // Relationships
        public int? FranchiseId { get; set; }
        public virtual Franchise? Franchise { get; set; }
        public virtual ICollection<Character>? Characters { get; set; }
    }
}
