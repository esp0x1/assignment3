﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models.Domain
{
    public class Character
    {
        // PK
        public int Id { get; set; }
        // Fields
        [MaxLength(255)]
        [Required]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Alias { get; set; }
        [MaxLength(255)]
        [Required]
        public string Gender { get; set; }
        [MaxLength(255)]
        [Required]
        public string Picture { get; set; }
        // Relationships
        public virtual ICollection<Movie>? Movies { get; set; }
    }
}
