﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models.Domain
{
    public class Franchise
    {
        // PK
        public int Id { get; set; }
        // Fields
        [MaxLength(255)]
        [Required]
        public string Name { get; set; }
        [MaxLength(255)]
        [Required]
        public string Description { get; set; }
        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
