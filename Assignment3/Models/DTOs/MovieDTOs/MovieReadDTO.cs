﻿using System.ComponentModel.DataAnnotations;
using Assignment3.Models.Domain;

namespace Assignment3.Models.DTOs.MovieDTOs
{
    public class MovieReadDTO
    {
        // PK
        public int Id { get; set; }
        // Fields
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public string? FranchiseId { get; set; }
        public List<string>? Characters { get; set; }
    }
}
