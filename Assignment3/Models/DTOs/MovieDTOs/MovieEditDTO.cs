﻿namespace Assignment3.Models.DTOs.MovieDTOs
{
    public class MovieEditDTO
    {
        // PK
        public int Id { get; set; }
        // Fields
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
    }
}
