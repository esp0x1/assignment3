﻿using System.ComponentModel.DataAnnotations;
using Assignment3.Models.Domain;

namespace Assignment3.Models.DTOs.FranchiseDTOs
{
    public class FranchiseCreateDTO
    {
        // Fields
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
