﻿using System.ComponentModel.DataAnnotations;
using Assignment3.Models.Domain;

namespace Assignment3.Models.DTOs.FranchiseDTOs
{
    public class FranchiseReadDTO
    {
        // PK
        public int Id { get; set; }
        // Fields
        public string Name { get; set; }
        public string Description { get; set; }
        // Relationships
        public List<string>? Movies { get; set; }
    }
}
