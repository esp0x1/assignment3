﻿namespace Assignment3.Models.DTOs.FranchiseDTOs
{
    public class FranchiseEditDTO
    {
        //PK
        public int Id { get; set; }
        // Fields
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
