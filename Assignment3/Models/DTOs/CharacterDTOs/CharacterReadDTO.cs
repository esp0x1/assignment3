﻿using Assignment3.Models.Domain;
using System.ComponentModel.DataAnnotations;

namespace Assignment3.Models.DTOs.CharacterDTOs
{
    public class CharacterReadDTO
    {
        // PK
        public int Id { get; set; }
        // Fields
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<string>? Movies { get; set; }
    }
}
