﻿namespace Assignment3.Models.DTOs.CharacterDTOs
{
    public class CharacterEditDTO
    {
        // PK
        public int Id { get; set; }
        // Fields
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
