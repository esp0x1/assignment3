﻿using Assignment3.Models.Domain;
using Assignment3.Models.DTOs.CharacterDTOs;
using AutoMapper;
namespace Assignment3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character -> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Title).ToArray()));

            // CharacterCreateDTO -> Character
            CreateMap<CharacterCreateDTO, Character>();

            // Character -> CharacterEditDTO
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
