﻿using Assignment3.Models.Domain;
using Assignment3.Models.DTOs.CharacterDTOs;
using Assignment3.Models.DTOs.MovieDTOs;
using AutoMapper;
namespace Assignment3.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie -> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Name).ToArray()));

            // MovieEditDTO -> Movie
            CreateMap<MovieEditDTO, Movie>();

            // MovieCreateDTO -> Movie
            CreateMap<MovieCreateDTO, Movie>();
        }
    }
}
