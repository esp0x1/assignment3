﻿using Assignment3.Models.Domain;
using Assignment3.Models.DTOs.CharacterDTOs;
using Assignment3.Models.DTOs.FranchiseDTOs;
using Assignment3.Models.DTOs.MovieDTOs;
using AutoMapper;
namespace Assignment3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // FranchiseCreateDTO -> Franchise
            CreateMap<FranchiseCreateDTO, Franchise>();

            // FranchiseEditDTO -> Franchise
            CreateMap<FranchiseEditDTO, Franchise>();

            // Franchise -> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Title).ToArray()));
        }
    }
}
