﻿using Assignment3.Models;
using Assignment3.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Assignment3.Services.MovieServices
{
    ///<inheritdoc[cref = "IMovieService"]/>
    public class MovieService : IMovieService
    {
        private readonly MovieCharactersDbContext _context;
        public MovieService(MovieCharactersDbContext context)
        {
            _context = context;
        }
        public async Task<Movie> AddMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<Movie> GetMovie(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Movie>> GetMovies()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }
        public async Task<IEnumerable<Character>> GetCharactersInMovie(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.Id == id))
                .ToListAsync();
        }

        public async Task PutMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task UpdateCharactersInMovie(int movieId, int[] characterIds)
        {
            Movie movie = _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(m => m.Id == movieId).Result;
            var domainCharacters = _context.Characters
                .Where(c => characterIds.Contains(c.Id))
                .ToListAsync().Result;

            if (domainCharacters.Count != characterIds.Length)
            {
                throw new KeyNotFoundException();
            }

            movie.Characters = domainCharacters;
            await _context.SaveChangesAsync();
        }
        public bool MovieExists(int id)
        {
            return (_context.Movies?.Any(m => m.Id == id)).GetValueOrDefault();
        }
    }
}
