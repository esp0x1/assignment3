﻿using Microsoft.AspNetCore.Mvc;
using Assignment3.Models.Domain;

namespace Assignment3.Services.MovieServices
{
    public interface IMovieService
    {
        /// <summary>
        /// Gets all movies
        /// </summary>
        public Task<IEnumerable<Movie>> GetMovies();
        /// <summary>
        /// Gets specific movie by ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        public Task<IEnumerable<Character>> GetCharactersInMovie(int id);
        /// <summary>
        /// Gets movie by ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        public Task<Movie> GetMovie(int id);
        /// <summary>
        /// Updates movie by a movie object
        /// </summary>
        /// <param name="movie">The updated movie</param>
        public Task PutMovie(Movie movie);
        /// <summary>
        /// Adds a new movie
        /// </summary>
        /// <param name="movie">The movie to add</param>
        public Task<Movie> AddMovie(Movie movie);
        /// <summary>
        /// Deletes a movie by ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        public Task DeleteMovie(int id);
        /// <summary>
        /// Updates characters in a specific movie
        /// </summary>
        /// <param name="movieId">The ID of the movie</param>
        /// <param name="characterIds">The characterIDs to add</param>
        public Task UpdateCharactersInMovie(int movieId, int[] characterIds);
        /// <summary>
        /// Checks if the movie exists
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        /// <returns>True if exists, else false</returns>
        public bool MovieExists(int id);

    }
}
