﻿using Assignment3.Models;
using Assignment3.Models.Domain;
using Assignment3.Models.DTOs.CharacterDTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Services.FranchiseServices
{
    ///<inheritdoc[cref = "IFranchiseService"]/>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharactersDbContext _context;
        public FranchiseService(MovieCharactersDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> AddFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task<Franchise> GetFranchise(int id)
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == id)
                .FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<Franchise>> GetFranchises()
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }

        public async Task PutFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.FranchiseId == id))
                .ToListAsync();
        }
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.FranchiseId == id)
                .ToListAsync();
        }
        public async Task UpdateMoviesInFranchise(int franchiseId, int[] movieIds)
        {
            Franchise franchise = _context.Franchises.Include(c => c.Movies).FirstOrDefaultAsync(m => m.Id == franchiseId).Result;
            var movies = _context.Movies
                .Where(m => movieIds.Contains(m.Id))
                .ToListAsync().Result;

            if(movies.Count != movieIds.Length)
            {
                throw new KeyNotFoundException();
            }
            //Set movies belonging to franchise
            franchise.Movies = movies;

            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return (_context.Franchises?.Any(f => f.Id == id)).GetValueOrDefault();
        }
    }
}
