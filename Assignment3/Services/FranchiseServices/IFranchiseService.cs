﻿using Microsoft.AspNetCore.Mvc;
using Assignment3.Models.Domain;

namespace Assignment3.Services.FranchiseServices
{
    public interface IFranchiseService
    {
        /// <summary>
        /// Gets all franchises
        /// </summary>
        public Task<IEnumerable<Franchise>> GetFranchises();
        /// <summary>
        /// Gets specific movie by ID
        /// </summary>
        /// <param name="id">The ID of the movie</param>
        public Task<Franchise> GetFranchise(int id);
        /// <summary>
        /// Updates franchise by a franchise object
        /// </summary>
        /// <param name="franchise">The updated franchise</param>
        public Task PutFranchise(Franchise franchise);
        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="franchise">The franchise to add</param>
        public Task<Franchise> AddFranchise(Franchise franchise);
        /// <summary>
        /// Gets characters in franchise by ID to franchise
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        public Task<IEnumerable<Character>> GetCharactersInFranchise(int id);
        /// <summary>
        /// Gets the movies in the franchise by ID to franchise
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        public Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);
        /// <summary>
        /// Deletes franchise by ID
        /// </summary>
        /// <param name="id">The ID of the franchise</param>
        /// <returns></returns>
        public Task DeleteFranchise(int id);
        /// <summary>
        /// Updates movies in franchise by ID and movieIDs
        /// </summary>
        /// <param name="franchiseId">The franchiseID</param>
        /// <param name="movieIds">The movieIDs to add</param>
        public Task UpdateMoviesInFranchise(int franchiseId, int[] movieIds);
        public bool FranchiseExists(int id);
    }
}
