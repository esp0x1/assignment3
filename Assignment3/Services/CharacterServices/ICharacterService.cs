﻿using Assignment3.Models.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Assignment3.Services.CharacterServices
{
    public interface ICharacterService
    {
        /// <summary>
        /// Gets all the characters
        /// </summary>
        public Task<IEnumerable<Character>> GetCharacters();
        /// <summary>
        /// Gets a specific character by ID
        /// </summary>
        /// <param name="id">The ID of the character</param>
        public Task<Character> GetCharacter(int id);
        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="character">The character to update</param>
        public Task PutCharacter(Character character);
        /// <summary>
        /// Adds a new character
        /// </summary>
        /// <param name="character">The character to add</param>
        public Task<Character> AddCharacter(Character character);
        /// <summary>
        /// Deletes the character
        /// </summary>
        /// <param name="id">The ID to delete</param>
        public Task DeleteCharacter(int id);
        /// <summary>
        /// Checks if the character exists
        /// </summary>
        /// <param name="id">The ID of the character to check</param>
        /// <returns>True if exists, else false</returns>
        public bool CharacterExists(int id);
    }
}
