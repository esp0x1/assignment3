﻿using Assignment3.Models.Domain;
using Assignment3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Services.CharacterServices
{
    ///<inheritdoc[cref = "ICharacterService"]/>
    public class CharacterService : ICharacterService
    {
        private readonly MovieCharactersDbContext _context;
        public CharacterService(MovieCharactersDbContext context)
        {
            _context = context;
        }
        public async Task<Character> AddCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        public async Task DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        public async Task<Character> GetCharacter(int id)
        {
             return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Character>> GetCharacters()
        {
            return await _context.Characters
                .Include(c=> c.Movies)
                .ToListAsync();
        }

        public async Task PutCharacter(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }
        public bool CharacterExists(int id)
        {
            return (_context.Characters?.Any(c => c.Id == id)).GetValueOrDefault();
        }
    }
}
