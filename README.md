# Assignment 2

Noroff Accelerate Assignment #3, Full-stack .NET development


# Prerequisites
### Automapper
### Automapper.extensions.microsoft.dependencyinjection
### EntityFrameworkCore.Design
### EntityFrameworkCore.Tools
### EntityFrameworkCore.SqlServer
<br/>

## This project contains the following
### ASP.NET Core Web Api, EF Code First:
<pre>
Models and Dbcontext catering for specifications in Appendix A from the assignment
Proper configuration of datatypes using data attributes
Comments on each of the classes showing where nevigation properties are and aspects of the DbContext
Connection string placed in appsettings.json instead of hardcoding into DbContext

Controllers and DTOs complying with specifications in Appendix B.
Swagger documentation using annotatinos
Summary tags for each method written.
</pre>



# Contributors
## Fredrik Fauskanger | Bergen, Norway
## Espen Sjo | Bergen, Norway

</br>

# Project Structure
```
.
├── Assignment3/
│   ├── Controllers/
│   │   ├── CharactersController.cs
│   │   ├── FranchisesController.cs
│   │   └── MoviesController.cs
│   ├── Migrations/
│   │   ├── 20220830113922_InitialDb.cs
│   │   └── MovieCharactersDbContextModelSnapshot.cs
│   ├── Models/
│   │   ├── Domain/
│   │   │   ├── Character.cs
│   │   │   ├── Franchise.cs
│   │   │   └── Movie.cs
│   │   ├── DTOs/
│   │   │   ├── CharacterDTOs/
│   │   │   │   ├── CharacterCreateDTO.cs
│   │   │   │   ├── CharacterEditDTO.cs
│   │   │   │   └── CharacterReadDto.cs
│   │   │   ├── FranchiseDTOs/
│   │   │   │   ├── FranchiseCreateDTO.cs
│   │   │   │   ├── FranchiseEditDTO.cs
│   │   │   │   └── FranchiseReadDTO.cs
│   │   │   └── MovieDTOs/
│   │   │       ├── MovieCreateDTO.cs
│   │   │       ├── MovieEditDTO.cs
│   │   │       └── MovieReadDTO.cs
│   │   ├── MovieCharactersDbContext.cs
│   │   └── SeedHelper.cs
│   ├── Profiles/
│   │   ├── CharacterProfile.cs
│   │   ├── FranchiseProfile.cs
│   │   └── MovieProfile.cs
│   ├── Properties/
│   │   └── launchSettings.json
│   ├── Services/
│   │   ├── CharacterServices/
│   │   │   ├── CharacterService.cs
│   │   │   └── ICharacterService.cs
│   │   ├── FranchiseServices/
│   │   │   ├── FranchiseService.cs
│   │   │   └── IFranchiseService.cs
│   │   └── MovieServices/
│   │       ├── MovieService.cs
│   │       └── IMovieService.cs
│   ├── appsettings.Development.json
│   ├── appsettings.json
│   └── Program.cs
├── .gitignore
├── Assignment3.sln
└── README.md

```
